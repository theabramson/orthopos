#!/usr/bin/env python
import matplotlib.pyplot as plt
plt.rcParams['svg.fonttype'] = 'none'

from typing import List, Optional
import argparse
import sys
import upsetplot 
from matplotlib import pyplot
from pandas import DataFrame, concat, Series

from misc import load_orthos


def get_ortho_counts(fpath: str) -> DataFrame:
    """ returns counts for orthogroups
    """
    tidy_og = load_orthos(fpath)
    og_counts =  tidy_og.groupby(["Orthogroup", "Sample"]).Protein.apply(len).unstack().fillna(0)
    return og_counts


def counts_to_intersections(
    counts: DataFrame,
    drop_false: bool=True,
    max_cols: Optional[int]=None,
    other_col: bool=False
) -> Series:
    """ Computes intersection sizes for some. Parameters...

        Counts :
            A dataframe from get_ortho_counts
        drop_false :
            whether or not to drop the False, False,..., False intersection
        max_cols :
            Maximum number of intersections to return
        other_col :
            Whether or not to add a column noting how many OG were removed by trimming to max_cols
    """
    bools = counts >= 1
    temp = bools.reset_index().groupby(list(bools.columns)).count()
    temp = temp.iloc[:, 0].sort_values(ascending=False)
    if(drop_false):
        false_key = (False,) * len(bools.columns)
        temp = temp.drop(false_key, errors="ignore")
    if(max_cols is not None):
        others = temp.iloc[max_cols:].sum()
        temp = temp.iloc[:max_cols]
        if(other_col):
            temp = concat([temp], keys=[False], names=["Others"])
            temp.loc[(True,) + (False,) * len(bools.columns)] = others
    return temp


def draw_upset(
    intersections: DataFrame,
    title: str,
    outpath: str
):
    """ Generates an upset plot
    """
    _ = upsetplot.plot(
        intersections,
        sort_by="cardinality",
        with_lines=False,
        show_counts=True
    )
    pyplot.suptitle(title)
    pyplot.savefig(outpath)


def get_args():
    """ parse command line args
    """
    parser = argparse.ArgumentParser("Generate Upset plots representing some orthofinder output")
    parser.add_argument("orthogroups", help="path to orthogroups.tsv file")
    parser.add_argument("-s", "--samples", nargs="+", help="the samples of interest")
    parser.add_argument("-n", "--names", nargs="*", help="the names to use for the samples")
    parser.add_argument("-t", "--title", default="Shared Orthogroup Sizes", help="the title to use")
    parser.add_argument("-o", "--outpath", default="orthoupset.svg", help="output image path")
    parser.add_argument("-c", "--columns", default=20, type=int, help="the maximum number of intersections/columns to plot")
    return parser.parse_args()


def main():
    args = get_args()
    if(args.names is None):
        args.names = args.samples
    if(len(args.names) != len(args.samples)):
        raise ValueError("Number of passed Names != Number of passed Samples")
    counts = get_ortho_counts(args.orthogroups)
    fails = {s for s in args.samples if(s not in counts.columns)}
    if(len(fails) != 0):
        raise ValueError(
            f"The following samples weren't found in {args.orthogroups} : {', '.join(fails)}"
        )
    counts = counts[args.samples]
    counts.columns = args.names
    intersections = counts_to_intersections(counts, max_cols=args.columns)
    draw_upset(intersections, args.title, args.outpath)


if(__name__ == "__main__"):
    main()
