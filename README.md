This repo contains a variety of scripts meant to work with orthofinder output. These scripts add the ability to embed genomic position information in orthofinder output and do statistical tests with orthofinder output. For the full description of arguments, use the "-h" or "--help" flags for the script you are interested in.

Each script be run in the same environment specified by "requirements.yml". To run the scripts, you will need an environment that satistifies all listed requirements. To create such an environment using conda:

```
git clone https://gitlab.com/NolanHartwick/orthopos.git
cd orthopos
conda env create -f requirements.yml -n your_env_name
conda activate your_env_name
```

### Orthopos

This is a simple script meant to add positional information to orthofinder output. This script accepts "Orthogroups.tsv", protein fasta files, and gff3 files. The protein fasta files must have an ID field in each contigs header allowing for mapping between the protein name in the given fasta file and the ID for the associated feature in the GFF3 file. In addition, no IDs or protein names should be duplicated anywhere in the dataset. To run, activate your environment and run something like the following command:

```
./orthopos.py /your/orthofinder/output/Orthogroups.tsv -f /your/fasta/files/*.fa -g /your/gff3/files/*.gff3 > Orthogroups.pos.tsv
```

### Orthostats

This script is meant to identify orthogroups that are enriched in some sample of interest. This script accepts "Orthogroups.tsv" and an identifier that should match one of the columns of "Orthogroups.tsv". To run, activate your environment and run something like the following command:

```
./orthostats.py /your/orthofinder/output/Orthogroups.tsv -s some_sample -o output
```

This will create two tables, "output.fdrs.tsv" and "output.ratios.tsv". The fdrs file displays, for each OG and each sample, the fdr corrected probability that that sample has the same number of genes as some_sample for that OG after correcting for gene_count differences. The ratios file displays, for each OG and each sample, the ratio between the number of genes for that sample and some_sample for that OG after correcting for gene count differences.

### Orthocomps

This script is meant to identify orthogroups that are enriched in some set of samples of interest relative to some other set. This script accepts "Orthogroups.tsv" and a set of specially formatted comparison identifiers. Each comparison should be formatted as "sample1,sample2:sample3,sample4" where sample 1,2,3, and 4 are all columns in Orthogroups.tsv and sample1 + sample2 corrosponds to one set of interest while sample3 + sample4 corrosponds to the other set of interest. To run, activate your environment and run something like the following command:

```
./orthocomps.py /your/orthofinder/output/Orthogroups.tsv -o output -c sample1:sample2,sample3 sample1,sample2,sample3:sample4,sample5
```

This will create two tables, "output.fdrs.tsv" and "output.ratios.tsv". The fdrs file displays, for each OG and each comparison, the fdr corrected probability that set1 has the same number of genes as set2 for that OG after correcting for gene_count differences. The ratios file displays, for each OG and each comparison, the ratio between the number of genes for set1 and set2 for that OG after correcting for gene count differences. These tables should have num_comparisons + 1 columns and num_OG + 1 rows. The extra row are the column headers. The extra column is the OG identifiers.

### Orthoupset

This script is meant to generate 'upset' plots representing a set of samples in some "Orthogroups.tsv" file. It expects the orthogroups file, a list of sample IDs that should match columns in the file, and can optionially be provided: a list of names to be used in place of the IDs in the plot, an output prefix controlling where plot will be written, and a plot title. To run, activate your environment and run something like the following command:

```
./orthoupset /your/orthofinder/output/Orthogroups.tsv -o output -s sample1 sample2 sample3 -n name1 name2 name3 -t my_title
```

This will generate a file named "output.png" representing the samples of interest. The y axis in the plot represents a count of OG. For two samples to "share" an OG, they both must have at least one gene belonging to that OG.
