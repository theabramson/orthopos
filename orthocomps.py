#!/usr/bin/env python
from typing import Tuple, Set
from pandas import read_csv, Series, DataFrame
from statsmodels.stats.multitest import multipletests
from scipy.stats import fisher_exact
import argparse
import sys

# from orthostats import get_ortho_counts
from misc import do_fisher_test, load_orthos


def do_fisher_comp(
    tidy_og: DataFrame,
    s1: Set[str],
    s2: Set[str]
) -> Tuple[Series, Series]:
    """ Do fisher test comparing samples s1 to samples s2
    """
    if(len(s2) < 5):
        print(f"Testing: {s1} vs {s2}")
    gene_counts = tidy_og.groupby("Sample").Protein.unique().apply(len)
    og_counts =  tidy_og.groupby(["Orthogroup", "Sample"]).Protein.apply(len).unstack().fillna(0)
    # print(og_counts.tail())
    # print(og_counts.shape)
    # print(og_counts.loc["GO:1904116"])
    og_counts = og_counts[(og_counts[s1].sum(axis=1)>0)|(og_counts[s2].sum(axis=1)>0)]
    # print(og_counts.shape)
    series1 = og_counts[s1].sum(axis=1)
    series2 = og_counts[s2].sum(axis=1)
    sum1 = gene_counts[s1].sum()
    sum2 = gene_counts[s2].sum()
    return do_fisher_test(series1, series2, sum1, sum2)


def parse_comp(s):
    """ parses the strings specifying a comparison to be made
    """
    sets = s.split(":")
    if(len(sets) != 2):
        raise ValueError("comparisons must specify exactly two sets. (only one ':')")
    sets = [s.split(",") for s in sets]
    return sets


def encode_comp(sets):
    """ inverse of parse_comp
    """
    sets = [",".join(s) for s in sets]
    return ":".join(sets)


def get_args():
    """ parse command line args
    """
    parser = argparse.ArgumentParser(
        "Uses fisher tests to make comparisons between sets of samples"
    )
    parser.add_argument("orthogroups", help="path to orthogroups.tsv file")
    parser.add_argument(
        "-c", "--comparisons",
        nargs="*",
        help=(
            "the comparisons to be made. Should be formatted as "
            "'sample1,sample2:sample3,sample4' to compare the set"
            "sample1 + sample2 to the set sample3 + sample4."
            "Default behavior is to compare each sample to all other samples"
        )
    )
    parser.add_argument("-o", "--outbase", help="prefix used for creating output tables")
    return parser.parse_args()


def main():
    args = get_args()
    tidy_og = load_orthos(args.orthogroups)
    samples = set(tidy_og.Sample.unique())
    if(args.comparisons is not None):
        comps = [parse_comp(c) for c in args.comparisons]
        comp_samples = [samp for comp in comps for s in comp for samp in s]
        fails = set(samp for samp in comp_samples if samp not in samples)
        if(len(fails) != 0):
            raise ValueError(
                f"The following samples aren't in {args.orthogroups} : {', '.join(fails)}"
            )
        cols = [encode_comp(c) for c in comps]
    else:
        # default comparisons
        cols = list(samples)
        comps = (({s}, samples - {s}) for s in cols)
    results = [do_fisher_comp(tidy_og, s1, s2) for s1, s2 in comps]
    suffixes = [".fdrs.tsv", ".ratios.tsv"]
    results = [
        DataFrame({c: r[i] for c, r in zip(cols, results)})
        for i, _ in enumerate(suffixes)
    ]
    for df, suf in zip(results, suffixes):
        df.to_csv(args.outbase + suf, sep="\t")


if(__name__ == "__main__"):
    main()
