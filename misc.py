from typing import Generator, Tuple, Set, TextIO, List
import pandas
from statsmodels.stats.multitest import multipletests
from scipy.stats import fisher_exact



def load_gff(f: str) -> pandas.DataFrame:
    """ Loads gff3 format file, partially and niavely parsing the attribute field
    """
    cols = "seqid source type start end score strand phase attr".split()
    df = pandas.read_csv(f, sep="\t", comment="#", header=None)
    df.columns = cols
    temp = df.attr.str.split(" *= *| *; *").apply(lambda l: dict(zip(l[::2], l[1::2])))
    df["Parent"] = temp.str["Parent"]
    df["ID"] = temp.str["ID"]
    df["attr_Name"] = temp.str["Name"]
    return df


def load_orthos(f: str) -> pandas.DataFrame:
    """ loads an orthogroups.tsv file from orthofinder
    """
    df = pandas.read_csv(f, sep="\t", index_col=0, dtype=str)
    df = df.stack().str.split(", *").reset_index()
    df_iter = zip(df.iloc[:, 0], df.iloc[:, 1], df.iloc[:, 2])
    ret = pandas.DataFrame(
        [[a, s, g] for a, s, gs in df_iter for g in gs],
        columns=["Orthogroup", "Sample", "Protein"]
    )
    return ret


def load_fasta(fpath: str, sep: str=" ") -> pandas.DataFrame:
    """ reads a fasta file at fpath and returns a dataframe representing the fasta
    """
    ret = []
    with open(fpath) as f:
        for header, seq in parse_fasta(f):
            split_header = header.split(sep)
            header_pairs = [s.split("=") for s in split_header[1:]]
            info = {k: v for k, v in header_pairs}
            info["name"] = header[0]
            info["seq"] = seq
            ret.append(info)
    return pandas.DataFrame(ret)


def parse_fasta(f: TextIO) -> Generator[Tuple[str, str], None, None]:
    """ Given a readable file like object containing fasta data, iterate
        over the fasta yielding a header, sequence pair for each entry

        >>> from io import StringIO
        >>> i = StringIO('>chr1\\nAAAAA\\n>chr2\\nCCCCC')
        >>> list(parse_fasta(i))
        [('chr1', 'AAAAA'), ('chr2', 'CCCCC')]
    """
    header = f.readline()[1:-1]
    if(header != ''):
        body : List[str] = []
        for line in f:
            if(line[0] == '>'):
                yield (header, ''.join(body))
                header = line[1:-1]
                body = []
            else:
                body.append(line.rstrip())
        yield (header, ''.join(body))


def do_fisher_test(
    series1: pandas.Series,
    series2: pandas.Series,
    sum1: int,
    sum2: int
) -> Tuple[pandas.Series, pandas.Series]:
    """ Do fisher test comparing counts from series1 to series2 given a total
        number of genes defined by sum1 and sum2
    """
    series2 = series2[series1.index]
    results = pandas.Series(
        [
            fisher_exact([[g1, sum1 - g1], [g2, sum2 - g2]])
            for g1, g2 in zip(
                series1,
                series2
            )
        ],
        index=series1.index
    )
    pvals = results.str[1]
    odds = results.str[0]
    fdrvals = multipletests(pvals, method="fdr_bh", alpha=0.05)[1]
    fdrvals = pandas.Series(fdrvals, index=pvals.index)
    return fdrvals, odds
