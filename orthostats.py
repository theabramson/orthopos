#!/usr/bin/env python
from typing import Tuple
import pandas
from statsmodels.stats.multitest import multipletests
from scipy.stats import fisher_exact
import argparse
import sys

from misc import load_orthos, do_fisher_test


def do_fishers(
    tidy_og: pandas.DataFrame,
    sample:str
) -> Tuple[pandas.DataFrame, pandas.DataFrame]:
    """ Do fisher tests comparing counts for sample to counts for everything
        else. Returns tuple of dataframes containing pvals, fdr values, and
        odds ratios, respectively.
    """
    gene_counts = tidy_og.groupby("Sample").Protein.unique().apply(len)
    og_counts =  tidy_og.groupby(["Orthogroup", "Sample"]).Protein.apply(len).unstack().fillna(0)
    pvals = {}
    oddsratio = {}
    for s in og_counts.columns:
        fdrs, ratios = do_fisher_test(
            og_counts[s],
            og_counts[sample],
            gene_counts[s],
            gene_counts[sample]
        )
        oddsratio[s] = ratios
        pvals[s] = fdrs
    return pandas.DataFrame(pvals), pandas.DataFrame(oddsratio)


def get_args():
    """ parse command line args
    """
    parser = argparse.ArgumentParser(
        "Uses fisher tests to compare a sample of interest to all other samples"
    )
    parser.add_argument("orthogroups", help="path to orthogroups.tsv file")
    parser.add_argument("-s", "--sample", help="the sample of interest")
    parser.add_argument("-o", "--outbase", help="prefix used for creating output tables")
    return parser.parse_args()


def main():
    args = get_args()
    tidy_og = load_orthos(args.orthogroups)
    if(args.sample not in tidy_og.Sample.unique()):
        raise ValueError(f"{args.sample} isn't in {args.orthogroups}")
    dfs = do_fishers(tidy_og, args.sample)
    suffixes = [".fdrs.tsv", ".ratios.tsv"]
    for df, suf in zip(dfs, suffixes):
        df.to_csv(args.outbase + suf, sep="\t")


if(__name__ == "__main__"):
    main()
